## 1.3.0 (2023-12-19)

### added (1 change)

- [Release FIPS image](gitlab-org/security-products/dast-runner-validation@c19e1e88582b5f0a88f6ef0d3f7f8236179ea347) ([merge request](gitlab-org/security-products/dast-runner-validation!22))

### other (1 change)

- [Build FIPS image](gitlab-org/security-products/dast-runner-validation@e68ff073b0bf0254d2a5b85b96970e0df96ef73f) ([merge request](gitlab-org/security-products/dast-runner-validation!21))

## 1.2.0 (2023-10-19)

### other (1 change)

- [Upgrade Alpine to version 3.18 to get latest dependencies](gitlab-org/security-products/dast-runner-validation@e82a3631988ff4686f5ce4a6727b539cb44187cd) ([merge request](gitlab-org/security-products/dast-runner-validation!20))

## 1.1.1 (2022-05-24)

### changed (1 change)

- [Add bundle frozen command to build process](gitlab-org/security-products/dast-runner-validation@8dd7b297ddecef4db908fadccc130f2bdfd11a57) ([merge request](gitlab-org/security-products/dast-runner-validation!13))

## 1.1.0 (2021-08-04)

### added (1 change)

- [Add meta tag validation strategy](gitlab-org/security-products/dast-runner-validation@f4f9d35851d18903da3316715b771ab5faf2c0b8) ([merge request](gitlab-org/security-products/dast-runner-validation!10))

### fixed (1 change)

- [Fix text file validation content type](https://gitlab.com/gitlab-org/security-products/dast-runner-validation/-/commit/9b1147db422e097d9fe860d7d70269d2c28b8b86) ([merge request](gitlab-org/security-products/dast-runner-validation!11))

## 1.0.0 (2021-07-13)

### changed (1 change)

- [Sync script with Rails application](gitlab-org/security-products/dast-runner-validation@5b4f90c8c35db9f9bc46b3e3054346386041f999) ([merge request](gitlab-org/security-products/dast-runner-validation!9))

### added (7 changes)

- [Add ability to create a release](gitlab-org/security-products/dast-runner-validation@2c548486b770186fc5d7b8f4fdfff3cbe40d79f0) ([merge request](gitlab-org/security-products/dast-runner-validation!8))
- [Add smoke test to .gitlab-ci.yml](gitlab-org/security-products/dast-runner-validation@baa8b7bcbf1d4bee7b4e27d33f76e8c7f258fb20) ([merge request](gitlab-org/security-products/dast-runner-validation!7))
- [Push intermediate images to container registry](gitlab-org/security-products/dast-runner-validation@1ade0cd958b1b25b4c387dab2fb97741d8601789) ([merge request](gitlab-org/security-products/dast-runner-validation!6))
- [Add files outlined by new project guidelines](gitlab-org/security-products/dast-runner-validation@b08f1448123379b598b2c19ff2b82f984220d867) ([merge request](gitlab-org/security-products/dast-runner-validation!3))
- [Add security checks to CI config](gitlab-org/security-products/dast-runner-validation@f6e602fc0150a202f04fc1a12d2a0fdf9e07ff0f) ([merge request](gitlab-org/security-products/dast-runner-validation!4))
- [Add script for performing DAST site validation](gitlab-org/security-products/dast-runner-validation@654be7fb8d4f3e722bb3a7b2f2819058eb304825) ([merge request](gitlab-org/security-products/dast-runner-validation!1))
- [Add LICENSE as per handbook guidelines](gitlab-org/security-products/dast-runner-validation@6c67a9ed7abb8398cb7fcf7340ee4283c2c94009) ([merge request](gitlab-org/security-products/dast-runner-validation!2))
