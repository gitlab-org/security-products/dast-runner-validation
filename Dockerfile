FROM alpine:3.18

USER root
RUN apk update && apk upgrade && apk add bash curl libxml2-utils
RUN addgroup -S dast && adduser -S dast -G dast

USER dast
WORKDIR /home/dast
ADD src/validate.sh .

CMD ./validate.sh
