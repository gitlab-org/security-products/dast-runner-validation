# frozen_string_literal: true

require 'concurrent-edge'
require 'sinatra'

class WebApp < Sinatra::Base
  MAX_MESSAGES = 50
  MESSAGES = Concurrent::Channel.new(capacity: MAX_MESSAGES)

  def self.take_all_messages
    MESSAGES.length.times.map { MESSAGES.take }
  end

  fail_count = 0
  post '/api/v4/internal/dast/site_validations/:id/transition' do
    if params['id'] == '1999' && fail_count < 2
      fail_count += 1
      status 524
      body 'This is a 500 error'
    else
      fail_count = 0
      message = { id: Integer(params['id']), event: JSON.parse(request.body.read)['event'], token: request.env['HTTP_JOB_TOKEN'] }

      Concurrent::Channel.go { MESSAGES.put(message) }
    end
  end

  get '/400_error' do
    status 400
    body 'This is a 400 error'
  end

  get '/500_error' do
    status 500
    body 'This is a 500 error'
  end

  get '/json' do
    content_type :json

    'validation-token'
  end

  get '/header' do
    response.headers['Validation-Header'] = 'validation-token'
  end

  get '/text_file' do
    response.headers['Content-Type'] = 'text/plain'

    'validation-token'
  end

  get '/meta_tag' do
    <<~EOS
      <!doctype html>
      <html lang="en">
        <head>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <meta name="gitlab-dast-validation" content="validation-token">
          <meta name="description" content="This really great website contains really great content.">
          <title>
            A really great website
          </title>
        </head>
        <body>
          <p>Really great content.</p>
        </body>
      </html>
    EOS
  end

  get '/redirect/:strategy' do
    redirect "/#{params[:strategy]}"
  end

  eventually_header_counter = 0
  get '/eventually_header' do
    eventually_header_counter += 1

    if eventually_header_counter > 2
      eventually_header_counter = 0

      response.headers['Validation-Header'] = 'validation-token'
    end
  end

  eventually_text_file_counter = 0
  get '/eventually_text_file' do
    eventually_text_file_counter += 1

    response.headers['Content-Type'] = 'text/plain'

    if eventually_text_file_counter > 2
      eventually_text_file_counter = 0

      'validation-token'
    end
  end

  eventually_meta_tag_counter = 0
  get '/eventually_meta_tag' do
    eventually_meta_tag_counter += 1

    if eventually_meta_tag_counter > 2
      eventually_meta_tag_counter = 0

      '<meta name="gitlab-dast-validation" content="validation-token">'
    end
  end
end
