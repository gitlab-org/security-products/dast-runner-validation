# frozen_string_literal: true

require 'spec_helper'

describe 'validate.sh' do
  let(:ci_job_token) { 'job-token' }
  let(:ci_server_url) { "http://localhost:#{@server.config[:Port]}" }
  let(:dast_site_validation_id) { rand(1...10) }
  let(:dast_site_validation_header) { 'Validation-Header' }
  let(:dast_site_validation_token) { 'validation-token' }
  let(:dast_site_validation_url) { "#{ci_server_url}/#{dast_site_validation_strategy}" }

  let(:max_retry) { '5' }
  let(:max_retry_delay) { '1' }

  let(:all_messages) { WebApp.take_all_messages }
  let(:retry_messages) { all_messages.select { |message| message[:event] == 'retry' } }
  let(:all_events) { all_messages.map { |message| message[:event] } }

  let(:default_env) do
    {
      'CI_JOB_TOKEN' => ci_job_token,
      'CI_SERVER_URL' => ci_server_url,
      'DAST_SITE_VALIDATION_ID' => String(dast_site_validation_id),
      'DAST_SITE_VALIDATION_HEADER' => dast_site_validation_header,
      'DAST_SITE_VALIDATION_STRATEGY' => dast_site_validation_strategy,
      'DAST_SITE_VALIDATION_TOKEN' => dast_site_validation_token,
      'DAST_SITE_VALIDATION_URL' => dast_site_validation_url,
      'MAX_RETRY' => max_retry,
      'MAX_RETRY_DELAY' => max_retry_delay
    }
  end

  let(:env) { default_env }

  subject do
    stdout, stderr, status = Open3.capture3(env, "#{Dir.pwd}/src/validate.sh")

    OpenStruct.new(stdout: stdout, stderr: stderr, status: status)
  end

  shared_examples 'it checks the env var' do |var|
    let(:dast_site_validation_strategy) { 'header' }

    context "when #{var} is missing" do
      let(:env) { default_env.tap { |hash| hash.delete(var) } }

      it 'communicates failure', :aggregate_failures do
        expect(subject.status).to_not be_success
        expect(subject.stderr).to include("#{var}: unbound variable")
      end
    end

    context "when #{var} is empty" do
      let(:env) { default_env.tap { |hash| hash[var] = '' } }

      it 'communicates failure', :aggregate_failures do
        expect(subject.status).to_not be_success
        expect(subject.stderr).to include("#{var}: must not be empty")
      end
    end
  end

  shared_examples 'a validation' do
    it 'tells gitlab it has started' do
      subject

      expect(all_messages.first).to eq(id: dast_site_validation_id, event: 'start', token: ci_job_token)
    end

    context 'when successful' do
      it 'communicates success', :aggregate_failures do
        expect(subject.status).to be_success
        expect(subject.stdout).to include('Validation success')

        expect(all_events).to eq(['start', 'pass'])

        expect(all_messages.last).to eq(id: dast_site_validation_id, event: 'pass', token: ci_job_token)
      end
    end

    shared_examples 'a failure' do
      it 'finally communicates failure', :aggregate_failures do
        expect(subject.status).to_not be_success
        expect(subject.stdout).to include('Validation failed: Retry limit exceeded')

        expect(all_events).to eq(expected_events)

        expect(retry_messages.length).to eq(Integer(max_retry))
        expect(all_messages.last).to eq(id: dast_site_validation_id, event: 'fail_op', token: ci_job_token)
      end
    end

    context 'when token is incorrect' do
      let(:dast_site_validation_token) { 'the-wrong-token' }

      it_behaves_like 'a failure' do
        let(:expected_events) { ['start', 'fail_op', 'retry', 'fail_op', 'retry', 'fail_op', 'retry', 'fail_op', 'retry', 'fail_op', 'retry', 'fail_op'] }
      end
    end

    context 'when tls certificate has expired' do
      let(:max_retry) { '0' }
      let(:dast_site_validation_url) { 'https://expired.badssl.com/' }

      it 'does not check the tls cert' do
        expect(subject.stderr).not_to include('SSL certificate problem')
      end
    end

    context 'when url redirects' do
      let(:dast_site_validation_url) { "#{ci_server_url}/redirect/#{dast_site_validation_strategy}" }

      it 'communicates success', :aggregate_failures do
        expect(subject.status).to be_success
        expect(subject.stdout).to include('Validation success')

        expect(all_messages.last).to eq(id: dast_site_validation_id, event: 'pass', token: ci_job_token)
      end
    end

    context 'when eventually successful' do
      let(:dast_site_validation_url) { "#{ci_server_url}/eventually_#{dast_site_validation_strategy}" }

      it 'eventually communicates success', :aggregate_failures do
        expect(subject.status).to be_success

        expect(retry_messages.length).to eq(2)
        expect(all_messages.last).to eq(id: dast_site_validation_id, event: 'pass', token: ci_job_token)
      end
    end

    context 'when eventually successful after an error connecting to transition api' do
      let(:dast_site_validation_id) { 1999 }

      it 'eventually communicates success', :aggregate_failures do
        expect(subject.status).to be_success

        expect(all_messages.last).to eq(id: dast_site_validation_id, event: 'pass', token: ci_job_token)
      end
    end
  end

  it_behaves_like 'it checks the env var', 'CI_JOB_TOKEN'
  it_behaves_like 'it checks the env var', 'CI_SERVER_URL'
  it_behaves_like 'it checks the env var', 'DAST_SITE_VALIDATION_ID'
  it_behaves_like 'it checks the env var', 'DAST_SITE_VALIDATION_HEADER'
  it_behaves_like 'it checks the env var', 'DAST_SITE_VALIDATION_STRATEGY'
  it_behaves_like 'it checks the env var', 'DAST_SITE_VALIDATION_TOKEN'
  it_behaves_like 'it checks the env var', 'DAST_SITE_VALIDATION_URL'

  context 'header validation' do
    let(:dast_site_validation_strategy) { 'header' }

    it_behaves_like 'a validation' do
      context 'when 400 status is returned' do
        let(:max_retry) { '0' }
        let(:dast_site_validation_url) { "#{ci_server_url}/400_error" }

        it_behaves_like 'a failure' do
          let(:expected_events) { ['start', 'fail_op'] }

          it 'communicates 400 status' do
            expect(subject.stdout).to include('HTTP/1.1 400 Bad Request')
          end
        end
      end
    end
  end

  context 'file validation' do
    let(:dast_site_validation_strategy) { 'text_file' }

    it_behaves_like 'a validation' do
      context 'when content type is incorrect' do
        let(:max_retry) { '0' }
        let(:dast_site_validation_url) { "#{ci_server_url}/json" }

        it_behaves_like 'a failure' do
          let(:expected_events) { ['start', 'fail_op'] }

          it 'communicates header mismatch' do
            expect(subject.stdout).to include('Validation failed: Expected content-type to be text/plain')
          end
        end
      end

      context 'when 500 status is returned' do
        let(:max_retry) { '0' }
        let(:dast_site_validation_url) { "#{ci_server_url}/500_error" }

        it_behaves_like 'a failure' do
          let(:expected_events) { ['start', 'fail_op'] }

          it 'communicates 500 status' do
            expect(subject.stdout).to include('HTTP/1.1 500 Internal Server Error')
          end
        end
      end
    end
  end

  context 'meta tag validation' do
    let(:dast_site_validation_strategy) { 'meta_tag' }

    it_behaves_like 'a validation'
  end

  context 'unknown validation strategy' do
    let(:dast_site_validation_strategy) { 'unknown' }

    it 'tells gitlab it has failed', :aggregate_failures do
      expect(subject.status).to_not be_success

      expect(all_messages.last).to eq(id: dast_site_validation_id, event: 'fail_op', token: ci_job_token)
    end
  end
end
