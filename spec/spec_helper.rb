# frozen_string_literal: true

require 'concurrent-edge'
require 'open3'
require 'ostruct'
require 'pry'
require 'rack'
require 'rack/handler/webrick'
require 'web_app'

RSpec.configure do |config|
  config.before(:all) do
    channel = Concurrent::Channel.new

    @server = WEBrick::HTTPServer.new(
      Port: 0,
      StartCallback: -> { Concurrent::Channel.go { channel.put(started: true) } },
      Logger: WEBrick::Log.new("#{Dir.pwd}/spec/log/mock.log"),
      AccessLog: [[File.open("#{Dir.pwd}/spec/log/mock_access.log", 'a'), WEBrick::AccessLog::COMBINED_LOG_FORMAT]]
    )

    @server.mount '/', Rack::Handler::WEBrick, WebApp

    Thread.new { @server.start }

    channel.take
  end

  config.after(:all) do
    @server.shutdown
  end

  config.before(:each) do
    WebApp.take_all_messages # clear channel
  end

  config.filter_run_when_matching :focus
end
