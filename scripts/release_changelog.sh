#!/usr/bin/env bash

set -Eeuo pipefail

function must_not_be_empty {
    echo "$1: must not be empty" >&2
    exit 1
}

[[ -z "$CI_COMMIT_MESSAGE" ]]    && must_not_be_empty "CI_COMMIT_MESSAGE"
[[ -z "$CI_COMMIT_SHA" ]]        && must_not_be_empty "CI_COMMIT_SHA"
[[ -z "$GITLAB_PRIVATE_TOKEN" ]] && must_not_be_empty "GITLAB_PRIVATE_TOKEN"

VERSION=$(echo "$CI_COMMIT_MESSAGE" | awk -F 'Add changelog for version ' '{print $2}')
TAG_NAME="v$VERSION"

DESCRIPTION=$(git diff --unified=0 "$CI_COMMIT_SHA"^! CHANGELOG.md | tail -n +7 | cut -c2-)
JSON_PAYLOAD=$(jq -n --arg description "$DESCRIPTION" '{ "description": $description }')

echo "Release: $TAG_NAME"
echo "$JSON_PAYLOAD"

echo "Updating release description"
curl --fail \
     --show-error \
     --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" \
     --header "Content-Type: application/json" \
     --data "$JSON_PAYLOAD" \
     --request PUT \
     "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/releases/$TAG_NAME"
