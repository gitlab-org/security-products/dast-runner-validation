#!/usr/bin/env bash

set -Eeuo pipefail

function error {
    echo "$1" >&2
    exit 1
}

function must_not_be_empty {
    error "$1: must not be empty"
}

[[ -z "$CI_COMMIT_SHA" ]]               && must_not_be_empty "CI_COMMIT_SHA"
[[ -z "$CI_PROJECT_ID" ]]               && must_not_be_empty "CI_PROJECT_ID"
[[ -z "$CI_REGISTRY" ]]                 && must_not_be_empty "CI_REGISTRY"
[[ -z "$CI_REGISTRY_PASSWORD" ]]        && must_not_be_empty "CI_REGISTRY_PASSWORD"
[[ -z "$CI_REGISTRY_USER" ]]            && must_not_be_empty "CI_REGISTRY_USER"
[[ -z "$DEPLOY_CI_REGISTRY_IMAGE" ]]    && must_not_be_empty "DEPLOY_CI_REGISTRY_IMAGE"
[[ -z "$DEPLOY_CI_REGISTRY_PASSWORD" ]] && must_not_be_empty "DEPLOY_CI_REGISTRY_PASSWORD"
[[ -z "$DEPLOY_CI_REGISTRY_USER" ]]     && must_not_be_empty "DEPLOY_CI_REGISTRY_USER"
[[ -z "$GITLAB_PRIVATE_TOKEN" ]]        && must_not_be_empty "GITLAB_PRIVATE_TOKEN"
[[ -z "$SOURCE_DOCKER_IMAGE" ]]         && must_not_be_empty "SOURCE_DOCKER_IMAGE"
[[ -z "$SOURCE_DOCKER_IMAGE_FIPS" ]]    && must_not_be_empty "SOURCE_DOCKER_IMAGE_FIPS"
[[ -z "$VERSION" ]]                     && must_not_be_empty "VERSION"

tag_and_push() {
    local source_image="$1"
    local tag="$2"
    local deploy_image="$DEPLOY_CI_REGISTRY_IMAGE:$tag"

    docker tag "$source_image" "$deploy_image"
    docker push "$deploy_image"
}

push_to_registry() {
    local source_image="$1"
    local suffix="$2"

    tag_and_push "$source_image" "$MAJOR.$MINOR.$PATCH$suffix"
    tag_and_push "$source_image" "$MAJOR.$MINOR$suffix"
    tag_and_push "$source_image" "$MAJOR$suffix"
    tag_and_push "$source_image" "latest$suffix"
}

BASE_URL="https://gitlab.com/api/v4/projects/$CI_PROJECT_ID"
TAG_NAME="v$VERSION"

echo "Checking tag"
if curl --silent --fail --show-error --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" "$BASE_URL/repository/tags/$TAG_NAME"; then
    error "Aborting, tag $TAG_NAME already exists. If this is not expected, please remove the tag and try again."
fi

echo "Pulling Docker images"
docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
docker pull "$SOURCE_DOCKER_IMAGE"
docker pull "$SOURCE_DOCKER_IMAGE_FIPS"

echo "Releasing Docker images"
docker login -u "$DEPLOY_CI_REGISTRY_USER" -p "$DEPLOY_CI_REGISTRY_PASSWORD" "$CI_REGISTRY"

MAJOR=$(echo "$VERSION" | awk -F '.' '{print $1}')
MINOR=$(echo "$VERSION" | awk -F '.' '{print $2}')
PATCH=$(echo "$VERSION" | awk -F '.' '{print $3}')

push_to_registry "$SOURCE_DOCKER_IMAGE" ""
push_to_registry "$SOURCE_DOCKER_IMAGE_FIPS" "-fips"

echo "Creating tag: $TAG_NAME"
curl --fail \
     --show-error \
     --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" \
     --request POST "$BASE_URL/repository/tags?tag_name=$TAG_NAME&ref=$CI_COMMIT_SHA"

echo "Creating release"
curl --fail \
     --show-error \
     --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" \
     --header "Content-Type: application/json" \
     --data "{ \"tag_name\": \"$TAG_NAME\", \"description\": \"See CHANGELOG.md\" }" \
     --request POST \
     "$BASE_URL/releases"

echo "Updating changelog"
curl --fail \
     --show-error \
     --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" \
     --data "version=$VERSION&to=$CI_COMMIT_SHA" \
     "$BASE_URL/repository/changelog"
