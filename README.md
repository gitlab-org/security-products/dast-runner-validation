# GitLab DAST Runner Validation

GitLab tool for validating [DAST site profiles]((https://docs.gitlab.com/ee/user/application_security/dast/#site-profile-validation))
from within a GitLab runner in [GitLab](https://gitlab.com/gitlab-org/gitlab).

## Development

### Build Image

```bash
docker build -t dast-runner-validation .
```

### Run Specs

```bash
bundle exec rspec
```

#### Notes

A recent version of [curl](https://curl.se/) is required. Mac users may install
this using Homebrew (e.g. `brew install curl`).

## Changelog

Please follow the same [changelog process](https://docs.gitlab.com/ee/development/changelog.html) as the Rails application.

## Release process

Please check the [release process](doc/release-process.md) documentation.

### Versioning

DAST Runner Validation uses [Semantic Versioning](https://semver.org/).

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md).
