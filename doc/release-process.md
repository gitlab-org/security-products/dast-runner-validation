# GitLab DAST Runner Validation Release Process

DAST Runner Validation uses [Semantic Versioning](https://semver.org/) to
communicate the kind of changes included in a release.

## Release Process

When changes are made to the repo, a CI Pipeline runs that builds two DAST
Runner Validation Docker images with the Docker tag of the Git commit SHA:
standard, and FIPS-enabled (with the "-fips" suffix). Once tests pass, a
maintainer may promote the images into production by triggering the `release`
CI Job. The maintainer must [specify a version](https://docs.gitlab.com/ee/ci/jobs/#specifying-variables-when-running-manual-jobs)
using the environment variable `VERSION` (e.g. `VERSION=1.0.0`).

The job will tag the built images as `latest` and with the specified `VERSION`.
It also tags the Git commit with the version and creates a new GitLab release.

The new Docker images should show up in the
[DAST Runner Validation Container Registry](https://gitlab.com/security-products/dast-runner-validation/container_registry).
