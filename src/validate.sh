#!/usr/bin/env bash

set -Eeuox pipefail

function must_not_be_empty {
    echo "$1: must not be empty" >&2
    exit 1
}

[[ -z "$CI_JOB_TOKEN" ]]                  && must_not_be_empty "CI_JOB_TOKEN"
[[ -z "$CI_SERVER_URL" ]]                 && must_not_be_empty "CI_SERVER_URL"
[[ -z "$DAST_SITE_VALIDATION_ID" ]]       && must_not_be_empty "DAST_SITE_VALIDATION_ID"
[[ -z "$DAST_SITE_VALIDATION_HEADER" ]]   && must_not_be_empty "DAST_SITE_VALIDATION_HEADER"
[[ -z "$DAST_SITE_VALIDATION_STRATEGY" ]] && must_not_be_empty "DAST_SITE_VALIDATION_STRATEGY"
[[ -z "$DAST_SITE_VALIDATION_TOKEN" ]]    && must_not_be_empty "DAST_SITE_VALIDATION_TOKEN"
[[ -z "$DAST_SITE_VALIDATION_URL" ]]      && must_not_be_empty "DAST_SITE_VALIDATION_URL"

# The FIPS image has an old curl that does not have "--retry-all-errors".
# If the image is updated to use a more recent curl we should remove this check, but in case we forget it will
# automatically start using the new flag.
if curl --help curl | grep -q "\--retry-all-errors";
then
  RETRY_ARG="--retry-all-errors"
else
  RETRY_ARG="--retry-connrefused"
fi

function call_gitlab {
    local url="$CI_SERVER_URL/api/v4/internal/dast/site_validations/$DAST_SITE_VALIDATION_ID/transition"

    curl --fail --retry 5 $RETRY_ARG --request POST --data "{\"event\":\"$1\"}" --header "JOB-TOKEN: $CI_JOB_TOKEN" "$url" --header "Content-Type: application/json"
}

function retry {
    local iteration=1
    local max_iterations="${MAX_RETRY:-5}"
    local retry_delay="${MAX_RETRY_DELAY:-15}"

    while true; do
        # shellcheck disable=SC2015
        "$@" && break || {
                if [[ $iteration -le $max_iterations ]]; then
                    ((iteration++))
                    echo "Validation failed: Retrying"
                    call_gitlab "fail_op"

                    sleep "$retry_delay"
                    call_gitlab "retry"
                else
                    echo "Validation failed: Retry limit exceeded"
                    call_gitlab "fail_op"

                    exit 1
                fi
            }
    done
}

function validation_curl {
    curl --location --insecure --dump-header "$@"
}

function header_validation {
    local tmp_file
    local response

    tmp_file=$(mktemp)
    response=$(validation_curl "$tmp_file" "$DAST_SITE_VALIDATION_URL")

    head -n 1 < "$tmp_file"

    grep -iq "$DAST_SITE_VALIDATION_HEADER: $DAST_SITE_VALIDATION_TOKEN" "$tmp_file"
}

function text_file_validation {
    local tmp_file
    local response

    tmp_file=$(mktemp)
    response=$(validation_curl "$tmp_file" "$DAST_SITE_VALIDATION_URL")

    head -n 1 < "$tmp_file"

    if ! grep -iq "content-type: text/plain" "$tmp_file";
    then
        echo "Validation failed: Expected content-type to be text/plain"
        return 1
    else
        echo "$response" | grep -q "$DAST_SITE_VALIDATION_TOKEN"
    fi
}

function meta_tag_validation {
    local tmp_file
    local response

    tmp_file=$(mktemp)
    response=$(validation_curl "$tmp_file" "$DAST_SITE_VALIDATION_URL")

    head -n 1 < "$tmp_file"

    echo "$response" | xmllint --html --xpath 'string(/html/head/meta[@name="gitlab-dast-validation"]/@content)' - | grep -q "$DAST_SITE_VALIDATION_TOKEN"
}

function validate {
    case "$DAST_SITE_VALIDATION_STRATEGY" in

        header)
            header_validation
            ;;

        text_file)
            text_file_validation
            ;;

        meta_tag)
            meta_tag_validation
            ;;

        *)
            echo "Validation failed: Invalid validation strategy"
            call_gitlab "fail_op"
            exit 1
            ;;
    esac
}

call_gitlab "start"

retry validate

call_gitlab "pass"

echo 'Validation success'
